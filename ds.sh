ds_exec() {
    docker exec $CONTAINER "$@"
}

get_root_of_dit() {
    ds_exec ldapsearch -H ldapi:/// -xLLL -s base -b "" namingContexts \
        | grep namingContexts: \
        | cut -d' ' -f2
}

get_root_dn() {
    ds_exec ldapsearch -H ldapi:/// -Y EXTERNAL -b "cn=config" -LLL -Q \
            "(olcDatabase=mdb)" olcRootDN \
        | grep olcRootDN: \
        | cut -d' ' -f2
}
