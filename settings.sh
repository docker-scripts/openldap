APP=openldap
PORTS="389:389"

### LDAP settings.
LDAP_DOMAIN="example.org"
LDAP_ORGANISATION="Example Inc."
LDAP_ADMIN_PASSWORD="pass123"

DOMAIN="ldap.$LDAP_DOMAIN"

SMTP_HOST="smtp.example.org"
MAIL_FROM="noreply@example.org"
