cmd_ou_help() {
    cat <<_EOF
    ou add <organizationalUnit>
    ou del <organizationalUnit>
    ou (list | ls)

_EOF
}

cmd_ou() {
    local cmd=$1
    case $cmd in
	add|del)
	    [[ -n $2 ]] || fail "Usage:\n$(cmd_ou_help)\n"
	    _ou_$cmd $2
	    ;;
	list|ls)
	    _ou_ls
	    ;;
	*)
	    fail "Usage:\n$(cmd_ou_help)\n"
	    ;;
    esac
}

_ou_add() {
    local ou=$1
    cat <<EOF | ds modify
dn: ou=$ou,$(get_root_of_dit)
changetype: add
objectClass: organizationalUnit
objectClass: top
ou: $ou
EOF
}

_ou_del() {
    local ou=$1
    cat <<EOF | ds modify
dn: ou=$ou,$(get_root_of_dit)
changetype: delete
EOF
}

_ou_ls() {
    ds search "(ou=*)" dn
}
