cmd_search() {
    cat <<_EOF
    search [...]
        Execute 'ldapsearch' inside the container.

_EOF
}

cmd_search() {
    ds_exec ldapsearch -H ldap:/// -xLLL \
            -D $(get_root_dn) \
            -w $LDAP_ADMIN_PASSWORD \
            -b $(get_root_of_dit) \
            "$@"
}
