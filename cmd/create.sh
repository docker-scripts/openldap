cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p slapd.d data
    orig_cmd_create \
        $([[ -n $DOMAIN ]] && echo "--hostname $DOMAIN") \
        --mount type=bind,src=$(pwd)/slapd.d,dst=/etc/ldap/slapd.d \
        --mount type=bind,src=$(pwd)/data,dst=/var/lib/ldap \
        "$@"    # accept additional options, e.g.: -p 2201:22
}
