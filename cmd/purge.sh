cmd_purge() {
    ds remove
    [[ -n $DOMAIN ]] && \
        rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
    rm -rf data/ slapd.d/ logs/
}
