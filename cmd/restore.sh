cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz>
        Restore from the given backup file.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"
    local backup=${file%%.tgz}
    backup=$(basename $backup)

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # stop slapd
    ds_exec systemctl stop slapd
    ds_exec killall slapd 2>/dev/null

    # remove existing config and data
    rm -rf slapd.d/* data/*
    
    # import the config and the data
    ds_exec slapadd -F /etc/ldap/slapd.d -n0 -l /host/$backup/config.ldif
    ds_exec slapadd -F /etc/ldap/slapd.d -l /host/$backup/data.ldif

    # fix permittions
    ds_exec chown -R openldap:openldap /etc/ldap/slapd.d/
    ds_exec chown -R openldap:openldap /var/lib/ldap/

    # start slapd
    ds_exec systemctl start slapd

    # clean up
    rm -rf $backup
}
