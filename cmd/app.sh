cmd_app_help() {
    cat <<_EOF
    app add <cn> <passwd>
    app del <cn>
    app (list | ls) [...]

    Apps are entries that have read-only access to all the data.

_EOF
}

cmd_app() {
    local cmd=$1 ; shift
    case $cmd in
        add|del)
            _app_$cmd "$@"
            ;;
        list|ls)
            _app_ls "$@"
            ;;
        *)
            fail "Usage:\n$(cmd_app_help)\n"
            ;;
    esac
}

_app_add() {
    local cn=$1 passwd=$2
    [[ -n $cn && -n $passwd ]] \
        || fail "Usage:\n$(cmd_app_help)\n"

    cat <<EOF | ds modify
dn: cn=$cn,ou=apps,$(get_root_of_dit)
changetype: add
objectClass: organizationalRole
objectClass: simpleSecurityObject
cn: $cn
userPassword: x
EOF
    ds set-passwd $passwd "cn=$cn,ou=apps,$(get_root_of_dit)"
}

_app_del() {
    local cn=$1
    [[ -n $cn ]] || fail "Usage:\n$(cmd_app_help)\n"

    cat <<EOF | ds modify
dn: cn=$cn,ou=apps,$(get_root_of_dit)
changetype: delete
EOF
}

_app_ls() {
    ds search -b "ou=apps,$(get_root_of_dit)" '(cn=*)' "$@"
}
