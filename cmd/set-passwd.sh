cmd_set-passwd_help() {
    cat <<_EOF
    set-passwd <passwd> <DN>
        Set a password to the given DN (distinguished name).

_EOF
}

cmd_set-passwd() {
    local passwd="$1"
    [[ -z $passwd ]] && fail "Usage:\n$(cmd_set-passwd_help)\n"

    local dn="$2"
    [[ -z $dn ]] && fail "Usage:\n$(cmd_set-passwd_help)\n"

    # set the password
    ds_exec ldappasswd -H ldap:/// -x \
            -D $(get_root_dn) -w "$LDAP_ADMIN_PASSWORD" \
            -s "$passwd" -S "$dn"
}
