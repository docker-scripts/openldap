cmd_modify_help() {
    cat <<_EOF
    ds modify <file.ldif>

    cat <<EOF | ds modify
    . . . . .
    EOF

        Execute 'ldapmodify' inside the container.

_EOF
}

cmd_modify() {
    local tmpfile="tmp-$RANDOM.ldif"
    cat $1 > $tmpfile
    ds_exec ldapmodify -H ldap:/// -x \
            -D $(get_root_dn) \
            -w "$LDAP_ADMIN_PASSWORD" \
            -f /host/$tmpfile
    rm -f $tmpfile
}
