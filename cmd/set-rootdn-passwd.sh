cmd_set-rootdn-passwd_help() {
    cat <<_EOF
    set-rootdn-passwd <passwd>
        Set this password to the RootDN (admin).

_EOF
}

cmd_set-rootdn-passwd() {
    local passwd="$1"
    [[ -z $passwd ]] && fail "Usage:\n$(cmd_set-rootdn-passwd_help)\n"

    local hashed_passwd=$(ds_exec slappasswd -h {SSHA} -s "$passwd")

    # set the new password in the config dit
    local tmpfile="tmp-$RANDOM.ldif"
    cat <<EOF > $tmpfile
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: $hashed_passwd
EOF
    ds_exec ldapmodify -H ldapi:/// -Q -Y EXTERNAL -f /host/$tmpfile
    rm $tmpfile
}
