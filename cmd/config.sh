cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds copy-ssl-cert
    _create_cron_job_to_copy_ssl_cert

    ds inject ubuntu-fixes.sh

    ds inject setup-slapd.sh
    ds set-rootdn-passwd "$LDAP_ADMIN_PASSWORD"

    # setup service-desk
    [[ -z $(ds search '(ou=apps)') ]] && ds ou add apps
    local passwd=$(tr -cd '[:alnum:]' < /dev/urandom | head -c20)
    [[ -z $(ds search '(cn=service-desk)') ]] && ds app add service-desk $passwd
    ds inject setup-service-desk.sh $passwd

    ds inject setup-self-service-password.sh
}

# create a cron job that copies the ssl cert from revproxy once a week
_create_cron_job_to_copy_ssl_cert() {
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir copy-ssl-cert &> /dev/null"
EOF
}
