cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # stop slapd
    ds_exec systemctl stop slapd
    ds_exec killall slapd 2>/dev/null

    # dump the config
    ds_exec slapcat -n0 -l /host/$backup/config.ldif

    # dump the data
    ds_exec slapcat -l /host/$backup/data.ldif

    # start slapd
    ds_exec systemctl start slapd

    # backup settings
    cp settings.sh $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
