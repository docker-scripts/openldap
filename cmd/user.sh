cmd_user_help() {
    cat <<_EOF
    user add <ou> <uid> <mail> [<passwd>]
    user del <ou> <uid>
    user (list | ls) [...]

_EOF
}

cmd_user() {
    local cmd=$1 ; shift
    case $cmd in
        add|del)
            _user_$cmd "$@"
            ;;
        list|ls)
            _user_ls "$@"
            ;;
        *)
            fail "Usage:\n$(cmd_user_help)\n"
            ;;
    esac
}

_user_add() {
    local ou=$1 uid=$2 mail=$3 passwd=$4
    [[ -n $ou && -n $uid && -n $mail ]] \
        || fail "Usage:\n$(cmd_user_help)\n"

    cat <<EOF | ds modify
dn: uid=$uid,ou=$ou,$(get_root_of_dit)
changetype: add
uid: $uid
objectClass: top
objectClass: inetOrgPerson
sn: $uid
cn: $uid
mail: $mail
EOF

    [[ -n $passwd ]] && \
        ds set-passwd $passwd "uid=$uid,ou=$ou,$(get_root_of_dit)"
}

_user_del() {
    local ou=$1 uid=$2
    [[ -n $ou && -n $uid ]] \
        || fail "Usage:\n$(cmd_user_help)\n"

    cat <<EOF | ds modify
dn: uid=$uid,ou=$ou,$(get_root_of_dit)
changetype: delete
EOF
}

_user_ls() {
    ds search "(uid=*)" "$@"
}
