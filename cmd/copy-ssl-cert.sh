cmd_copy-ssl-cert_help() {
    cat <<_EOF
    copy-ssl-cert
        Copy the SSL cert from 'revproxy/letsencrypt/' to 'sslcert/'

_EOF
}

cmd_copy-ssl-cert() {
    [[ -z $DOMAIN ]] && return

    local ssl_cert_dir=$CONTAINERS/revproxy/letsencrypt/live/$DOMAIN
    mkdir -p sslcert
    cp -fL $ssl_cert_dir/* sslcert/
    ds_exec chown -R openldap: /host/sslcert/
}
