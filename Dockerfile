include(jammy)

RUN DEBIAN_FRONTEND=noninteractive \
        apt install --yes slapd ldap-utils
RUN cp -a /etc/ldap /etc/ldap.bak
RUN apt install --yes \
        wget curl gnupg debconf-utils ca-certificates

### install service-desk and self-service-password
### https://service-desk.readthedocs.io/en/stable/installation.html#debian-ubuntu
### https://self-service-password.readthedocs.io/en/latest/installation.html#debian-ubuntu
RUN curl https://ltb-project.org/documentation/_static/RPM-GPG-KEY-LTB-project | gpg --dearmor > /usr/share/keyrings/ltb-project.gpg && \
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/ltb-project.gpg] https://ltb-project.org/debian/stable stable main' >> /etc/apt/sources.list.d/ltb-project.list && \
    apt update && \
    DEBIAN_FRONTEND=noninteractive \
        apt install --yes \
            service-desk \
            self-service-password=1.5.4-1 \
            php-mbstring \
            smarty3

### install a newer version of smarty3
RUN wget http://ftp.us.debian.org/debian/pool/main/s/smarty3/smarty3_3.1.47-2_all.deb && \
    dpkg -i smarty3_3.1.47-2_all.deb &&  rm smarty3_3.1.47-2_all.deb
