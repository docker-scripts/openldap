# OpenLDAP container

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull openldap`

  - Create a directory for the container: `ds init openldap @openldap`

  - Customize settings: `cd /var/ds/openldap/ ; vim settings.sh`

  - Make the container: `ds make`

## Usage

- Add organizational units (under the root of DIT):

  ```
  ds ou add users
  ds ou add groups
  ds ou add apps
  ds ou ls
  ```

- Add users:

  ```
  ds user add users user1 user1@example.org pass123
  ds user add users user2 user2@example.org
  ds set-passwd pass234 uid=user2,ou=users,dc=example,dc=org
  ds user ls
  ds user del users user1
  ```

- Add app credentials:

  ```
  ds app add test1 pass123
  ds app add mail1 pass234
  ds app add gitea pass345
  ds app ls
  ds app del test1
  ```

  These app credentials look like `cn=test1,ou=apps,dc=example,dc=org`
  and they have read-only access to the data (the normal users can
  just authenticate with their own password and change their own
  password, and nothing else).

- Search:

  ```
  ds search "(uid=user*)" dn
  ```
  
- Backup and restore:

  ```
  ds backup
  ds restore backup-20201217.tgz
  ```
  
- Modify:

  ```
  ds modify file.ldif
  ```
  
  ```
  cat <<EOF | ds modify
  dn: uid=user3,ou=users,dc=example,dc=org
  changetype: modify
  replace: mail
  mail: xyz@example.com
  ```

- Customize:

  Most probably the data structure of your users is different from
  what `ds user` assumes. To customize it, make a local copy of
  `cmd/user.sh` and modify it accordingly:

  ```
  cd /var/ds/openldap/
  mkdir -p cmd/
  cp /opt/docker-scripts/openldap/cmd/user.sh cmd/
  vim cmd/user.sh
  ```

- To reset user passwords from a web interface, open
  https://ldap.example.org/service/ and login with username `admin`
  and the admin password (which is the value of `LDAP_ADMIN_PASSWORD`
  on `settings.sh`).

## Other commands

```
ds stop
ds start
ds shell
ds help
ds remove
ds purge
```
