#!/bin/bash -x

source /host/settings.sh

main() {
    # fix config files
    if [[ ! -f '/etc/ldap/slapd.d/cn=config.ldif' ]]; then
        # initialize config
        cp -a /etc/ldap.bak/slapd.d/* /etc/ldap/slapd.d
        chown -R openldap:openldap /etc/ldap

        # reconfigure slapd with the correct settings
        slapd-reconfigure
        # allow to read only the children of ou=apps (by default all the users can read)
        fix-access-control
    fi
    rm -rf /etc/ldap.bak

    # set TLS certificates
    [[ -n $DOMAIN ]] && set-tls-certificates
}

slapd-reconfigure() {
    cat <<EOF | debconf-set-selections
slapd slapd/domain string $LDAP_DOMAIN
slapd shared/organization string '$LDAP_ORGANISATION'
slapd slapd/password1 password $LDAP_ADMIN_PASSWORD
slapd slapd/password2 password $LDAP_ADMIN_PASSWORD
slapd slapd/move_old_database boolean true
slapd slapd/purge_database boolean false
slapd slapd/no_configuration boolean false
EOF
    echo -e '#!/bin/sh\nexit 0' > /usr/sbin/policy-rc.d
    dpkg-reconfigure -f noninteractive slapd

    # restart slapd
    killall -q slapd
    systemctl restart slapd
}

fix-access-control() {
    local root_of_dit=$(ldapsearch -H ldapi:/// -xLLL -s base -b "" namingContexts \
                            | grep namingContexts \
                            | cut -d' ' -f2)
    cat <<EOF | ldapmodify -Y EXTERNAL -H ldapi:///
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: to attrs=userPassword
    by self write
    by anonymous auth
-
add: olcAccess
olcAccess: to dn.subtree="$root_of_dit"
    by dn.children="ou=apps,$root_of_dit" read
EOF
}

set-tls-certificates() {
    cat <<EOF | ldapmodify -Y EXTERNAL -H ldapi:///
dn: cn=config
replace: olcTLSCACertificateFile
olcTLSCACertificateFile: /host/sslcert/fullchain.pem
-
replace: olcTLSCertificateFile
olcTLSCertificateFile: /host/sslcert/cert.pem
-
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /host/sslcert/privkey.pem

EOF

    # restart slapd
    killall slapd
    systemctl restart slapd
}

# start main
main "$@"
