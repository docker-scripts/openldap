#!/bin/bash -x

source /host/settings.sh

main() {
    local password=$1

    mkdir -p /host/config/
    setup-apache2 $password
    setup-service-desk
}

get_ldap_base() {
    ldapsearch -H ldapi:/// -xLLL -s base -b "" namingContexts \
        | grep namingContexts: \
        | cut -d' ' -f2
}

get_admin_dn() {
    ldapsearch -H ldapi:/// -Y EXTERNAL -b "cn=config" -LLL -Q \
               "(olcDatabase=mdb)" olcRootDN \
        | grep olcRootDN: \
        | cut -d' ' -f2
}

setup-apache2() {
    local auth_ldap_bind_password=$1
    local ldap_base=$(get_ldap_base)
    local admin_dn=$(get_admin_dn)

    local config_file=/host/config/service-desk.apache2.conf
    [[ -f $config_file ]] || cat <<EOF > $config_file
Alias /service /usr/share/service-desk/htdocs

<Directory /usr/share/service-desk/htdocs>
    AllowOverride None
    AuthType basic
    AuthName "$LDAP_ORGANISATION / LDAP Service Desk"
    AuthBasicProvider ldap
    AuthLDAPURL "ldap://localhost/$ldap_base?cn?one"
    AuthLDAPBindDN cn=service-desk,ou=apps,$ldap_base
    AuthLDAPBindPassword $auth_ldap_bind_password
    #Require ldap-user admin
    Require ldap-dn $admin_dn
    ### For more details see:
    ### https://httpd.apache.org/docs/current/mod/mod_authnz_ldap.html
</Directory>
EOF

    ln -s $config_file /etc/apache2/conf-enabled/service-desk.conf

    a2ensite default-ssl
    a2dissite 000-default
    a2enmod ssl authnz_ldap

    systemctl restart apache2
}

setup-service-desk() {
    local ldap_base=$(get_ldap_base)
    local admin_dn=$(get_admin_dn)

    local config_file=/host/config/service-desk.inc.php
    [[ -f $config_file ]] || cat <<EOF > $config_file
<?php

// For more details see: https://service-desk.readthedocs.io

\$ldap_url = "ldap://localhost:389";
\$ldap_starttls = true;
\$ldap_binddn = "$admin_dn";
\$ldap_bindpw = "$LDAP_ADMIN_PASSWORD";
\$ldap_base = "$ldap_base";
\$ldap_user_base = "ou=users,".\$ldap_base;
\$ldap_user_filter = "(objectClass=inetOrgPerson)";
\$ldap_size_limit = 100;

\$use_checkpassword = true;
\$use_resetpassword = true;
\$resetpassword_reset_default = false;
\$use_lockaccount = false;
\$use_unlockaccount = false;
EOF

    ln -s $config_file /usr/share/service-desk/conf/config.inc.local.php

    systemctl restart apache2
}

# start main
main "$@"
