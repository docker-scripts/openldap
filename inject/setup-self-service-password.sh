#!/bin/bash -x

source /host/settings.sh

main() {
    mkdir -p /host/config/
    setup-apache2
    setup-self-service-password
}

get_ldap_base() {
    ldapsearch -H ldapi:/// -xLLL -s base -b "" namingContexts \
        | grep namingContexts: \
        | cut -d' ' -f2
}

get_admin_dn() {
    ldapsearch -H ldapi:/// -Y EXTERNAL -b "cn=config" -LLL -Q \
               "(olcDatabase=mdb)" olcRootDN \
        | grep olcRootDN: \
        | cut -d' ' -f2
}

setup-apache2() {
    local config_file=/host/config/self-service-password.apache2.conf
    [[ -f $config_file ]] || cat <<EOF > $config_file
<Directory /usr/share/self-service-password/htdocs>
    AllowOverride None
    Require all granted
    AddDefaultCharset UTF-8
</Directory>

<Directory /usr/share/self-service-password/scripts>
    AllowOverride None
    Require all denied
</Directory>
EOF

    sed -i /etc/apache2/sites-available/default-ssl.conf \
        -e '\%DocumentRoot% c DocumentRoot /usr/share/self-service-password/htdocs'
    sed -i /etc/apache2/sites-available/default-ssl.conf \
        -e "\%DocumentRoot% a Include $config_file"

    a2ensite default-ssl
    a2dissite 000-default
    a2enmod ssl

    systemctl restart apache2
}

setup-self-service-password() {
    local ldap_base=$(get_ldap_base)
    local admin_dn=$(get_admin_dn)
    local keyphrase=$(tr -cd '[:alnum:]' < /dev/urandom | head -c100)

    local config_file=/host/config/self-service-password.inc.php
    [[ -f $config_file ]] || cat <<EOF > $config_file
<?php

// For more details see: https://self-service-password.readthedocs.io

\$keyphrase = "$keyphrase";
\$obscure_failure_messages = array("mailnomatch");

\$ldap_url = "ldap://localhost:389";
\$ldap_starttls = true;
\$ldap_binddn = "$admin_dn";
\$ldap_bindpw = "$LDAP_ADMIN_PASSWORD";
\$ldap_base = "$ldap_base";
\$ldap_login_attribute = "uid";
\$ldap_fullname_attribute = "cn";
\$ldap_filter = "(&(objectClass=inetOrgPerson)(\$ldap_login_attribute={login}))";

\$use_questions = false;
\$use_sms = false;

\$hash = "auto";
# \$who_change_password = "manager";

\$mail_smtp_host = '$SMTP_HOST';
\$mail_from = "$MAIL_FROM";
\$notify_on_change = true;

$use_captcha = true;

# \$logo = "";
# \$background_image = "";
EOF

    ln -s $config_file /usr/share/self-service-password/conf/config.inc.local.php

    systemctl restart apache2
}

# start main
main "$@"
